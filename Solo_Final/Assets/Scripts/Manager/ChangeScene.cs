﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ChangeScene : MonoBehaviour
{
    public void NextScene()
    {
        SceneManager.LoadScene("Stage 2");
    }
    public void Back()
    {
        SceneManager.LoadScene("Game");
    }
}
